package com.github.search.models;

/**
 * Created by santo on 6/12/17.
 */

public class Repo {

private String avtar_url;
private int watchers_count;
private int forks_count;
private String repo_name;
private String full_name;
private int stargazers_count;
private String project_link;
private String repo_desc;
private String repo_contributors_url;
private double score;

    public String getAvtarUrl() {
        return avtar_url;
    }

    public void setAvtarUrl(String avtar_url) {
        this.avtar_url = avtar_url;
    }

    public int getWatchersCount() {
        return watchers_count;
    }

    public void setWatchersCount(int watchers_count) {
        this.watchers_count = watchers_count;
    }

    public int getForksCount() {
        return forks_count;
    }

    public void setForksCount(int commit_count) {
        this.forks_count = forks_count;
    }

    public String getRepoName() {
        return repo_name;
    }

    public void setRepoName(String repo_name) {
        this.repo_name = repo_name;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public int getStargazersCount() {
        return stargazers_count;
    }

    public void setStargazersCount(int stargazers_count) {
        this.stargazers_count = stargazers_count;
    }

    public String getProjectLink() {
        return project_link;
    }

    public void setProjectLink(String project_link) {
        this.project_link = project_link;
    }

    public String getRepoDesc() {
        return repo_desc;
    }

    public void setRepoDesc(String repo_desc) {
        this.repo_desc = repo_desc;
    }

    public String getRepoContributorsUrl() {
        return repo_contributors_url;
    }

    public void setRepoContributorsUrl(String repo_contributors_url) {
        this.repo_contributors_url = repo_contributors_url;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }
}
