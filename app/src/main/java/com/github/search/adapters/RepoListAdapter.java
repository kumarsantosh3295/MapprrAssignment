package com.github.search.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.search.R;
import com.github.search.activities.RepoDetailActivity;
import com.github.search.models.Repo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by santo on 6/12/17.
 */

public class RepoListAdapter extends RecyclerView.Adapter<RepoListAdapter.ViewHolder> {

    private List<Repo> repos;
    private Context context;

    public RepoListAdapter(Context context) {
        this.repos = new ArrayList<>();
        this.context = context;
    }

    public void updateList(List<Repo> repos) {
        this.repos.clear();
        this.repos.addAll(repos);
        notifyDataSetChanged();
    }

    @Override
    public RepoListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RepoListAdapter.ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_row, parent, false));
    }

    @Override
    public void onBindViewHolder(RepoListAdapter.ViewHolder holder, int position) {
        final Repo repo = repos.get(position);
        if(repo != null) {
            holder.repo_name.setText(repo.getRepoName());
            holder.full_name.setText(repo.getFull_name());
            holder.forks_count.setText(String.valueOf(repo.getForksCount()));
            holder.stargazers_count.setText(String.valueOf(repo.getStargazersCount()));
            holder.watchers_count.setText(String.valueOf(repo.getWatchersCount()));
            Picasso.with(context).load(repo.getAvtarUrl()).into(holder.avtar_image);
            holder.container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, RepoDetailActivity.class);
                    intent.putExtra("avtarUrl",repo.getAvtarUrl());
                    intent.putExtra("name",repo.getRepoName());
                    intent.putExtra("projectLink",repo.getProjectLink());
                    intent.putExtra("repoDesc",repo.getRepoDesc());
                    intent.putExtra("contributorsUrl",repo.getRepoContributorsUrl());
                    context.startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return repos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView repo_name, full_name, watchers_count, forks_count, stargazers_count;
        private ImageView avtar_image;
        private RelativeLayout container;

        public ViewHolder(View itemView) {
            super(itemView);
            avtar_image =(ImageView) itemView.findViewById(R.id.avtar);
            repo_name =(TextView) itemView.findViewById(R.id.repo_name);
            full_name =(TextView) itemView.findViewById(R.id.full_name);
            watchers_count =(TextView) itemView.findViewById(R.id.watchers_count);
            forks_count =(TextView) itemView.findViewById(R.id.forks_count);
            stargazers_count =(TextView) itemView.findViewById(R.id.stargazers_count);
            container = (RelativeLayout) itemView.findViewById(R.id.repo_item);
        }
    }
}