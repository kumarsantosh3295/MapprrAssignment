package com.github.search.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.search.R;
import com.github.search.activities.ContributorDetail;
import com.github.search.models.Contributors;
import com.github.search.utils.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by santo on 7/12/17.
 */

public class ContributorsAdapter extends RecyclerView.Adapter<ContributorsAdapter.ViewHolder> {

    private Context context;
    private List<Contributors> contributorsList;

    public ContributorsAdapter(Context context) {
        this.context = context;
        contributorsList = new ArrayList<>();
    }

    public void updateList(List<Contributors> contributorsList) {
        this.contributorsList.clear();
        this.contributorsList.addAll(contributorsList);
        notifyDataSetChanged();
    }

    @Override
    public ContributorsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ContributorsAdapter.ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contributor_image, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Contributors contributor = contributorsList.get(position);
        if (contributor != null) {
            Picasso.with(context).load(contributor.getAvtarUrl()).into(holder.imageView);
            holder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ContributorDetail.class);
                    intent.putExtra("avtarUrl", contributor.getAvtarUrl());
                    intent.putExtra("name", contributor.getContributorName());
                    intent.putExtra("repoListUrl", contributor.getRepoListUrl());
                    context.startActivity(intent);
                }
            });
        }
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return contributorsList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        CircularImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = (CircularImageView) itemView.findViewById(R.id.image_view);
        }
    }
}

