package com.github.search.webservices;

import android.os.AsyncTask;
import android.util.Log;

import com.github.search.models.Contributors;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by santo on 7/12/17.
 */

public class ContributorListTask extends AsyncTask<Void, Void, List<Contributors>>{

    private ContributorListListener listListener;
    private String url;

    public ContributorListTask(ContributorListListener listListener,String url) {
        this.listListener = listListener;
        this.url = url;
    }

    @Override
    protected List<Contributors> doInBackground(Void... params) {
        try {
            String response = WebUtils.getServiceData(url,null);
            Log.d("tag",response);
            if (response != null) {
                JSONArray jsonArray = new JSONArray(response);
                            if (jsonArray != null) {
                                List<Contributors> list = new ArrayList<>();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    Contributors contributors = new Contributors();
                                    contributors.setAvtarUrl(jsonObject.getString("avatar_url"));
                                    contributors.setContributorName(jsonObject.getString("login"));
                                    contributors.setRepoListUrl(jsonObject.getString("repos_url"));
                                    list.add(contributors);
                                }
                                return list;
                            }
                        }
        } catch (Exception e) {
            Log.d("tag","Exception in parsing");
        }
        return null;
    }

    @Override
    protected void onPostExecute(List<Contributors> list) {
        super.onPostExecute(list);

        if (list != null && list.size() > 0) {
            listListener.onSuccess(list);

        } else {
            listListener.onError();
        }
    }

    public interface ContributorListListener {

        void onSuccess(List<Contributors> repo);

        void onError();
    }
}
