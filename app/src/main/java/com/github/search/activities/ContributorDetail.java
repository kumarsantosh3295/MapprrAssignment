package com.github.search.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;
import com.github.search.R;
import com.github.search.adapters.RepoListAdapter;
import com.github.search.models.Repo;
import com.github.search.utils.CircularImageView;
import com.github.search.utils.Utility;
import com.github.search.webservices.QueryRepoTask;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ContributorDetail extends AppCompatActivity {

    private RecyclerView contributor_list;
    private RepoListAdapter repoListAdapter;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contributor_detail);

        contributor_list = (RecyclerView) findViewById(R.id.contributor_list);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        contributor_list.setLayoutManager(layoutManager);
        repoListAdapter = new RepoListAdapter(this);
        contributor_list.setAdapter(repoListAdapter);
        Intent intent = getIntent();

        CollapsingToolbarLayout collapsingLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbar);
        collapsingLayout.setTitle(intent.getStringExtra("name"));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_black);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        CircularImageView avtarImage = (CircularImageView) findViewById(R.id.avtar_image);
        Picasso.with(this).load(intent.getStringExtra("avtarUrl")).into(avtarImage);
        getRepository(intent.getStringExtra("repoListUrl"));
    }

    private void getRepository(String url) {
        if (Utility.isNetwork(this)) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            QueryRepoTask repoTask = new QueryRepoTask(repoListener, null, url);
            repoTask.execute();
        } else {
            Toast.makeText(this, "No network connection!", Toast.LENGTH_SHORT).show();
        }
    }

    QueryRepoTask.QueryRepoListener repoListener = new QueryRepoTask.QueryRepoListener() {
        @Override
        public void onSuccess(List<Repo> repo) {
            repoListAdapter.updateList(repo);
            progressDialog.dismiss();
        }

        @Override
        public void onError() {
            Toast.makeText(ContributorDetail.this, "Something went wrong", Toast.LENGTH_LONG).show();
            progressDialog.dismiss();
        }
    };
}
