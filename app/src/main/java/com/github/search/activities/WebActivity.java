package com.github.search.activities;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.github.search.R;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by santo on 7/12/17.
 */

public class WebActivity extends AppCompatActivity {
    public static final String EXTRA_URL_STRING = "url_string";
    String urlString;
    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_PROGRESS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_bar_webview);
        progressBar.setIndeterminate(false);
        urlString = getIntent().getStringExtra(EXTRA_URL_STRING);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_black);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        webView = (WebView) findViewById(R.id.payment_web_view);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        if (savedInstanceState == null) {
            webView.loadUrl(urlString);
        } else {
            webView.restoreState(savedInstanceState);
        }

        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                progressBar.setProgress(progress);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
                    if (progress == 100) {
                        progressBar.animate().alpha(0f);
                    } else {
                        progressBar.setAlpha(1f);
                    }
                } else {
                    if (progress == 100) {
                        progressBar.setVisibility(View.GONE);
                    } else {
                        progressBar.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
        webView.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                try {
                    JSONObject props = new JSONObject();
                    Long tsLong = System.currentTimeMillis() / 1000;
                    String ts1 = tsLong.toString();
                    try {
                        props.put("timestamp", ts1);
                        props.put("url", failingUrl);
                    } catch (JSONException e) {
                        Log.e("RegisterActivity", "unexpected Json exception", e);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onPageFinished(WebView view, String url) {
                try {
                    WebActivity.this.setTitle(view.getTitle());

                    JSONObject props = new JSONObject();
                    Long tsLong = System.currentTimeMillis() / 1000;
                    String ts1 = tsLong.toString();
                    try {
                        props.put("timestamp", ts1);
                        props.put("url", url);
                     } catch (JSONException e) {
                        Log.e("RegisterActivity", "unexpected Json exception", e);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return super.shouldOverrideUrlLoading(view, url);
            }
        });


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        webView.saveState(outState);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        // overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
    }




}