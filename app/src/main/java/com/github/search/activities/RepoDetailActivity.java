package com.github.search.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.github.search.R;
import com.github.search.adapters.ContributorsAdapter;
import com.github.search.models.Contributors;
import com.github.search.utils.CircularImageView;
import com.github.search.utils.Utility;
import com.github.search.webservices.ContributorListTask;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RepoDetailActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;
    private RecyclerView gridView;
    private ContributorsAdapter adapter;
    private CollapsingToolbarLayout collapsingLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repo_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();

        collapsingLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbar);
        collapsingLayout.setTitle(intent.getStringExtra("name"));
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_black);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        CircularImageView avtarImage = (CircularImageView) findViewById(R.id.avtar_image);
        Picasso.with(this).load(intent.getStringExtra("avtarUrl")).into(avtarImage);
        final TextView project_link = (TextView) findViewById(R.id.project_link);
        TextView desc = (TextView) findViewById(R.id.desc);
        project_link.setText(Html.fromHtml(intent.getStringExtra("projectLink")));
        project_link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RepoDetailActivity.this, WebActivity.class);
                intent.putExtra("url_string", project_link.getText().toString());
                startActivity(intent);
            }
        });

        desc.setText(intent.getStringExtra("repoDesc"));
        getContributors(intent.getStringExtra("contributorsUrl"));

        gridView = (RecyclerView) findViewById(R.id.grid_view);
        gridView.setLayoutManager(new GridLayoutManager(this, 4));
        adapter = new ContributorsAdapter(this);
        gridView.setAdapter(adapter);
    }

    private void getContributors(String url) {
        if (Utility.isNetwork(this)) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            ContributorListTask listTask = new ContributorListTask(listListener, url);
            listTask.execute();
        } else {
            Toast.makeText(this, "No network connection!", Toast.LENGTH_SHORT).show();
        }
    }

    ContributorListTask.ContributorListListener listListener = new ContributorListTask.ContributorListListener() {
        @Override
        public void onSuccess(List<Contributors> list) {
            adapter.updateList(list);
            progressDialog.dismiss();
        }

        @Override
        public void onError() {
            progressDialog.dismiss();
            Toast.makeText(RepoDetailActivity.this, "Something went wrong", Toast.LENGTH_LONG).show();
        }
    };
}
