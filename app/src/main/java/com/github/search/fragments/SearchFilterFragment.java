package com.github.search.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.allattentionhere.fabulousfilter.AAH_FabulousFragment;
import com.github.search.R;
import com.github.search.utils.Constant;
import com.github.search.utils.SharedPreferenceManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by santo on 8/12/17.
 */

public class SearchFilterFragment extends AAH_FabulousFragment implements View.OnClickListener {

    private Calendar myCalendar;
    private boolean fromDate, toDate;
    private TextView fromDatetxt, toDatetxt;
    private Button starsbtn, forksbtn, updatedbtn, descbtn, ascbtn, scorebtn;
    private String sort_by, from_date, to_date, order_by;
    private SharedPreferenceManager sharedPreferenceManager;

    public static SearchFilterFragment newInstance() {
        SearchFilterFragment f = new SearchFilterFragment();
        return f;
    }

    public void createDatePicker() {
        new DatePickerDialog(getContext(), date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {

        try {

            View contentView = View.inflate(getContext(), R.layout.filter_fragment, null);
            myCalendar = Calendar.getInstance();
            sharedPreferenceManager = SharedPreferenceManager.getInstance(getContext());


            RelativeLayout rl_content = (RelativeLayout) contentView.findViewById(R.id.rl_content);
            LinearLayout ll_TextViews = (LinearLayout) contentView.findViewById(R.id.ll_TextViews);
            fromDatetxt = (TextView) contentView.findViewById(R.id.from_date_txt);
            toDatetxt = (TextView) contentView.findViewById(R.id.to_date_txt);
            starsbtn = (Button) contentView.findViewById(R.id.stars);
            scorebtn = (Button) contentView.findViewById(R.id.score);
            forksbtn = (Button) contentView.findViewById(R.id.forks);
            updatedbtn = (Button) contentView.findViewById(R.id.updated);
            descbtn = (Button) contentView.findViewById(R.id.desc);
            ascbtn = (Button) contentView.findViewById(R.id.asc);
            starsbtn.setOnClickListener(this);
            forksbtn.setOnClickListener(this);
            updatedbtn.setOnClickListener(this);
            descbtn.setOnClickListener(this);
            ascbtn.setOnClickListener(this);
            scorebtn.setOnClickListener(this);

            contentView.findViewById(R.id.from).setOnClickListener(this);
            contentView.findViewById(R.id.to).setOnClickListener(this);

            if (sharedPreferenceManager != null) {
                String date_Range = sharedPreferenceManager.getDateRange(Constant.DATE_RANGE);
                String order_by = sharedPreferenceManager.getOrderBy(Constant.ORDER_BY);
                String sort_by = sharedPreferenceManager.getSortBy(Constant.SORT_BY);
                if (date_Range != null && !date_Range.isEmpty()) {
                    Log.d("tag", date_Range);
                    String arr[] = date_Range.split("\\..");
                    from_date = arr[0];
                    to_date = arr[1];
                    fromDatetxt.setText(from_date);
                    toDatetxt.setText(to_date);
                }
                if (order_by != null && !order_by.isEmpty()) {
                    switch (order_by) {
                        case "desc":
                            descbtn.setBackgroundResource(R.drawable.button_border);
                            break;
                        case "asc":
                            ascbtn.setBackgroundResource(R.drawable.button_border);
                            break;
                    }
                }
                if (sort_by != null && !sort_by.isEmpty()) {
                    switch (sort_by) {
                        case "stars":
                            Log.d("tag", "stars");
                            starsbtn.setBackgroundResource(R.drawable.button_border);
                            break;
                        case "forks":
                            forksbtn.setBackgroundResource(R.drawable.button_border);
                            break;
                        case "updated":
                            updatedbtn.setBackgroundResource(R.drawable.button_border);
                            break;
                        case "score":
                            scorebtn.setBackgroundResource(R.drawable.button_border);
                            break;
                    }
                }
            }

            contentView.findViewById(R.id.save).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        if (sharedPreferenceManager != null) {
                            if (sort_by != null && !sort_by.isEmpty())
                                sharedPreferenceManager.saveData(Constant.SORT_BY, sort_by);

                            if (order_by != null && !order_by.isEmpty())
                                sharedPreferenceManager.saveData(Constant.ORDER_BY, order_by);

                            if (from_date != null && !from_date.isEmpty()) {
                                if (to_date != null && !to_date.isEmpty()) {
                                    String myFormat = "yyyy-MM-dd";
                                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                                    Date today = new Date(System.currentTimeMillis());
                                    if (sdf.parse(from_date).before(sdf.parse(to_date)) && today.after(sdf.parse(to_date))) {
                                        sharedPreferenceManager.saveData(Constant.DATE_RANGE, from_date + ".." + to_date);
                                    } else {
                                        Toast.makeText(getContext(), "check the range of date or last date", Toast.LENGTH_LONG).show();
                                        return;
                                    }
                                } else {
                                    Toast.makeText(getContext(), "Please provide valide date", Toast.LENGTH_LONG).show();
                                    return;
                                }
                            } else if (to_date != null && !to_date.isEmpty()) {
                                Toast.makeText(getContext(), "Please provide valide date", Toast.LENGTH_LONG).show();
                                return;
                            }
                        }
                        closeFilter("closed");
                    } catch (Exception e) {
                    }
                }
            });

            contentView.findViewById(R.id.reset).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (sharedPreferenceManager != null) {
                        sharedPreferenceManager.saveData(Constant.DATE_RANGE, "");
                        sharedPreferenceManager.saveData(Constant.ORDER_BY, "");
                        sharedPreferenceManager.saveData(Constant.SORT_BY, "");
                    }
                    closeFilter("closed");
                }
            });

            contentView.findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    closeFilter("closed");
                }
            });

            setViewgroupStatic(ll_TextViews);
            setViewMain(rl_content);
            setMainContentView(contentView);
            super.setupDialog(dialog, style);
        } catch (Exception e) {
        }
    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        Log.d("tag", sdf.format(myCalendar.getTime()));
        if (fromDate) {
            fromDatetxt.setText(sdf.format(myCalendar.getTime()));
            from_date = sdf.format(myCalendar.getTime());
        } else if (toDate) {
            toDatetxt.setText(sdf.format(myCalendar.getTime()));
            to_date = sdf.format(myCalendar.getTime());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.from:
                fromDate = true;
                toDate = false;
                createDatePicker();
                break;
            case R.id.to:
                toDate = true;
                fromDate = false;
                createDatePicker();
                break;
            case R.id.stars:
                starsbtn.setBackgroundResource(R.drawable.button_border);
                forksbtn.setBackgroundResource(R.color.white);
                updatedbtn.setBackgroundResource(R.color.white);
                scorebtn.setBackgroundResource(R.color.white);
                sort_by = "stars";
                break;
            case R.id.forks:
                starsbtn.setBackgroundResource(R.color.white);
                forksbtn.setBackgroundResource(R.drawable.button_border);
                updatedbtn.setBackgroundResource(R.color.white);
                scorebtn.setBackgroundResource(R.color.white);
                sort_by = "forks";
                break;
            case R.id.score:
                scorebtn.setBackgroundResource(R.drawable.button_border);
                starsbtn.setBackgroundResource(R.color.white);
                forksbtn.setBackgroundResource(R.color.white);
                updatedbtn.setBackgroundResource(R.color.white);
                sort_by = "score";
                break;
            case R.id.updated:
                starsbtn.setBackgroundResource(R.color.white);
                forksbtn.setBackgroundResource(R.color.white);
                updatedbtn.setBackgroundResource(R.drawable.button_border);
                scorebtn.setBackgroundResource(R.color.white);
                sort_by = "updated";
                break;
            case R.id.desc:
                descbtn.setBackgroundResource(R.drawable.button_border);
                ascbtn.setBackgroundResource(R.color.white);
                order_by = "desc";
                break;
            case R.id.asc:
                descbtn.setBackgroundResource(R.color.white);
                ascbtn.setBackgroundResource(R.drawable.button_border);
                order_by = "asc";
                break;
        }
    }
}
