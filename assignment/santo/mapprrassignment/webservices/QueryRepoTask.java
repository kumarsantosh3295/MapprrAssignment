package com.assignment.santo.mapprrassignment.webservices;

import android.os.AsyncTask;
import android.util.Log;

import com.assignment.santo.mapprrassignment.models.Repo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by santo on 6/12/17.
 */

public class QueryRepoTask extends AsyncTask<Void, Void, List<Repo>> {

    private HashMap<String, String> query_params;
    private QueryRepoListener repoListener;
    private String url;
    public QueryRepoTask(QueryRepoListener repoListener, HashMap<String, String> query_params, String url) {
        this.repoListener = repoListener;
        this.query_params = query_params;
        this.url = url;
    }

    @Override
    protected List<Repo> doInBackground(Void... params) {
        try {
            String response = null;
            if(url !=null && query_params == null)
                response = WebUtils.getServiceData(url,null);
            if(url == null && query_params !=null)
                response = WebUtils.getServiceData(WebServiceConstants.SEARCH_REPO, query_params);

            Log.d("tag",response);
            if (response != null) {

                if(url ==null) {
                    JSONObject jsonObject = new JSONObject(response);

                    if (jsonObject != null) {

                        if (jsonObject.has("incomplete_results") && !jsonObject.getBoolean("incomplete_results")) {
                            List<Repo> repoList = new ArrayList<>();
                            if (jsonObject.has("items")) {
                                JSONArray repoJsonList = jsonObject.getJSONArray("items");
                                if (repoList != null) {
                                    for (int i = 0; i < repoJsonList.length(); i++) {
                                        JSONObject repoItem = repoJsonList.getJSONObject(i);
                                        Repo repo = new Repo();
                                        repo.setRepoName(repoItem.getString("name"));
                                        repo.setFull_name(repoItem.getString("full_name"));
                                        repo.setWatchersCount(repoItem.getInt("watchers_count"));
                                        repo.setForksCount(repoItem.getInt("forks_count"));
                                        repo.setStargazersCount(repoItem.getInt("stargazers_count"));
                                        repo.setProjectLink(repoItem.getString("html_url"));
                                        repo.setRepoDesc(repoItem.getString("description"));
                                        repo.setRepoContributorsUrl(repoItem.getString("contributors_url"));
                                        if (repoItem.has("owner")) {
                                            JSONObject ownerObj = repoItem.getJSONObject("owner");
                                            repo.setAvtarUrl(ownerObj.getString("avatar_url"));
                                        }
                                        repoList.add(repo);
                                    }
                                    return repoList;
                                }
                            }
                        }
                    }
                } else {
                    JSONArray repoJsonList = new JSONArray(response);
                    if (repoJsonList != null) {
                        List<Repo> repoList = new ArrayList<>();
                        for (int i = 0; i < repoJsonList.length(); i++) {
                            JSONObject repoItem = repoJsonList.getJSONObject(i);
                            Repo repo = new Repo();
                            repo.setRepoName(repoItem.getString("name"));
                            repo.setFull_name(repoItem.getString("full_name"));
                            repo.setWatchersCount(repoItem.getInt("watchers_count"));
                            repo.setForksCount(repoItem.getInt("forks_count"));
                            repo.setStargazersCount(repoItem.getInt("stargazers_count"));
                            repo.setProjectLink(repoItem.getString("html_url"));
                            repo.setRepoDesc(repoItem.getString("description"));
                                repo.setRepoContributorsUrl(repoItem.getString("contributors_url"));
                            if (repoItem.has("owner")) {
                                JSONObject ownerObj = repoItem.getJSONObject("owner");
                                repo.setAvtarUrl(ownerObj.getString("avatar_url"));
                            }

                            repoList.add(repo);
                        }
                        return repoList;
                }
            }
        } } catch (Exception e) {
            Log.d("tag","Exception in parsing");
        }
        return null;
    }

    @Override
    protected void onPostExecute(List<Repo> repos) {
        super.onPostExecute(repos);

        if (repos != null && repos.size() > 0) {
            repoListener.onSuccess(repos);

        } else {
            repoListener.onError();
        }
 }

    public interface QueryRepoListener {

        void onSuccess(List<Repo> repo);

        void onError();

    }
}
