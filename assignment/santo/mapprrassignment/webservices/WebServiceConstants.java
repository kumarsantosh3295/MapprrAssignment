package com.assignment.santo.mapprrassignment.webservices;

/**
 * Created by santo on 6/12/17.
 */

public class WebServiceConstants {

    public static final String SERVICE_BASE_URL =  "https://api.github.com/";
    public static final String SEARCH_REPO = SERVICE_BASE_URL + "search/repositories";

}
