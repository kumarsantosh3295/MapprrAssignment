package com.assignment.santo.mapprrassignment.models;

/**
 * Created by santo on 7/12/17.
 */

public class Contributors {

    private String avtar_url;
    private String contributor_name;
    private String repo_list_url;

    public String getAvtarUrl() {
        return avtar_url;
    }

    public void setAvtarUrl(String avtar_url) {
        this.avtar_url = avtar_url;
    }

    public String getRepoListUrl() {
        return repo_list_url;
    }

    public void setRepoListUrl(String repo_list_url) {
        this.repo_list_url = repo_list_url;
    }

    public String getContributorName() {
        return contributor_name;
    }

    public void setContributorName(String contributor_name) {
        this.contributor_name = contributor_name;
    }
}
