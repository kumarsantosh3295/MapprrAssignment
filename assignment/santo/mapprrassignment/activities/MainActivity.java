package com.assignment.santo.mapprrassignment.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.assignment.santo.mapprrassignment.R;
import com.assignment.santo.mapprrassignment.adapters.RepoListAdapter;
import com.assignment.santo.mapprrassignment.fragments.SearchFilterFragment;
import com.assignment.santo.mapprrassignment.models.Repo;
import com.assignment.santo.mapprrassignment.utils.Constant;
import com.assignment.santo.mapprrassignment.utils.SharedPreferenceManager;
import com.assignment.santo.mapprrassignment.utils.Utility;
import com.assignment.santo.mapprrassignment.webservices.QueryRepoTask;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private ProgressDialog progressDialog;
    private RecyclerView queryRepoList;
    private RepoListAdapter repoListAdapter;
    private String sort_by;
    private String date_range;
    private String order_by;
    SharedPreferenceManager sharedPreferenceManager;
    SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        queryRepoList = (RecyclerView) findViewById(R.id.query_repo_list);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        queryRepoList.setLayoutManager(layoutManager);
        repoListAdapter = new RepoListAdapter(this);
        queryRepoList.setAdapter(repoListAdapter);
        sharedPreferenceManager = SharedPreferenceManager.getInstance(this);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.container);
        mSwipeRefreshLayout.setOnRefreshListener(this);


        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SearchFilterFragment dialogFrag = SearchFilterFragment.newInstance();
                dialogFrag.setParentFab(fab);
                dialogFrag.show(getSupportFragmentManager(), dialogFrag.getTag());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!TextUtils.isEmpty(query)) {
                    getRepository(query);
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }
        });
        return true;
    }

    private void getRepository(String query) {
        if (Utility.isNetwork(this)) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            HashMap<String, String> paramsData = new HashMap<>();
            if (sharedPreferenceManager != null) {
                String data_range = sharedPreferenceManager.getDateRange(Constant.DATE_RANGE);
                if (data_range != null && !data_range.isEmpty())
                    paramsData.put("q", query + "+created:" + data_range);
                else
                    paramsData.put("q", query);
            } else
                paramsData.put("q", query);

            if (sharedPreferenceManager != null) {
                String sort_by = sharedPreferenceManager.getDateRange(Constant.SORT_BY);
                if (sort_by != null && !sort_by.isEmpty())
                    paramsData.put("sort", sort_by);
            }
            if (sharedPreferenceManager != null) {
                String order_by = sharedPreferenceManager.getDateRange(Constant.ORDER_BY);
                if (order_by != null && !order_by.isEmpty())
                    paramsData.put("order", order_by);
            }
            paramsData.put("per_page", "10");
            QueryRepoTask repoTask = new QueryRepoTask(repoListener, paramsData, null);
            repoTask.execute();
        } else {
            Toast.makeText(this, "No network connection!", Toast.LENGTH_SHORT).show();
        }
    }

    QueryRepoTask.QueryRepoListener repoListener = new QueryRepoTask.QueryRepoListener() {
        @Override
        public void onSuccess(List<Repo> repo) {
            arrangeItme(repo);
            progressDialog.dismiss();
        }

        @Override
        public void onError() {
            progressDialog.dismiss();
            Toast.makeText(MainActivity.this, "Something went wrong", Toast.LENGTH_LONG).show();
        }
    };

    //this method sort the list
    private void arrangeItme(List<Repo> repos) {
        Collections.sort(repos, new Comparator<Repo>() {
            @Override
            public int compare(Repo lhs, Repo rhs) {
                return (rhs.getWatchersCount() - lhs.getWatchersCount());
            }
        });
        repoListAdapter.updateList(repos);
    }

    @Override
    public void onRefresh() {
     Toast.makeText(this,"huuree",Toast.LENGTH_LONG).show();
    }
}
