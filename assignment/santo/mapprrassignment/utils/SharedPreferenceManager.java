package com.assignment.santo.mapprrassignment.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by santo on 8/12/17.
 */


public class SharedPreferenceManager {
    private static SharedPreferences mPreference;
    public static SharedPreferenceManager preferenceManager;
    private static SharedPreferences.Editor editor;

    private SharedPreferenceManager() {
    }

    private static synchronized void initPreference(Context context) {
        if (mPreference == null) {
            mPreference = context.getSharedPreferences(Constant.MAPPRR_PREFERENCE, Context.MODE_PRIVATE);
            editor = mPreference.edit();
        }
    }

    public static synchronized SharedPreferenceManager getInstance(Context context) {
        if (preferenceManager == null) {
            preferenceManager = new SharedPreferenceManager();
        }
        initPreference(context);
        return preferenceManager;
    }

    public void saveData(String key, String data) {
        mPreference.edit().putString(key, data).apply();
    }

    public String getSortBy(String key) {
        try {
            return mPreference.getString(key, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getOrderBy(String key) {
        try {
            return mPreference.getString(key, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getDateRange(String key) {
        try {
            return mPreference.getString(key, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
