package com.assignment.santo.mapprrassignment.utils;

/**
 * Created by santo on 5/12/17.
 */

public class Constant {

    //App constants
    public static final String MAPPRR_PREFERENCE = "com.assignment.santo.mapprrassignment.app.shared_preference";
    public static final String SORT_BY="sort_By";
    public static final String ORDER_BY = "order_By";
    public static final String DATE_RANGE = "date_Range";
}
